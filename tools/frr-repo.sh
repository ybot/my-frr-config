#!/bin/bash
set -Eeuo pipefail

curl -s --fail https://deb.frrouting.org/frr/keys.asc | apt-key add -
echo deb https://deb.frrouting.org/frr stable frr-stable | tee -a /etc/apt/sources.list.d/frr.list
apt update

# dependencies from bullseye
curl -LO --fail 'http://ftp.us.debian.org/debian/pool/main/j/json-c/libjson-c3_0.12.1+ds-2_amd64.deb'
curl -LO --fail 'http://ftp.us.debian.org/debian/pool/main/r/readline/libreadline7_7.0-5_amd64.deb'

dpkg -i libreadline7_7.0-5_amd64.deb libjson-c3_0.12.1+ds-2_amd64.deb
apt install frr frr-pythontools
