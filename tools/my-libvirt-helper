#!/bin/bash
set -Eeuo pipefail
: ${MY_LIBVIRT_TEMPLATE:=/lib/mygw/my-libvirt.tmpl}
: ${MY_LIBVIRT_CEPH_ID:="00000000-0000-0000-0000-000000000001"}
: ${MY_CEPH_USER:="libvirt"}
: ${MY_CEPH_POOL:="libvirt-vms"}
: ${MY_CEPH_BASEPOOL:="libvirt-base-images"}

MY_NAME="${1%.mygw}"
MY_CONF="${1:-}"
boot="${2:-}"


[ ! -e "${MY_CONF}" ] && echo "not a file" && exit 1

. ${MY_CONF}

: ${MY_NICS:=$(grep "^MY_NICS_" ${MY_CONF} | sed 's/^MY_NICS_\([[:alnum:]]*\)_.*/\1/' | sort | uniq | tr '[:upper:]' '[:lower:]')}

[ -n "${boot}" ] && boot="<boot dev='network'/>"

for nic in ${MY_NICS}; do
	[ "${nic}" == "lo" ] && continue

	mac=MY_NICS_${nic^^}_MAC
	vlan=MY_NICS_${nic^^}_TRUNK
	vxlan=MY_NICS_${nic^^}_VXLAN
	unnumbered=MY_NICS_${nic^^}_IPV6
	mac="${!mac:-}"
	vlan="${!vlan:-}"
	vxlan="${!vxlan:-}"
	unnumbered="${!unnumbered:-}"

	[ -z "${mac}" ] && mac=$(printf '52%010x\n' $(date +%s) | sed 's/../&:/g;s/:$//')

	if [ "${unnumbered}" == "unnumbered" ]; then
		script="qemu-ifup-public"
		id=pub
	fi
	if [ -n "${vlan}" ]; then
		script="qemu-ifup-${vlan%.*}"
		id=$(printf '%x' ${vlan##*.} 2>/dev/null) || id=${vlan}
	fi
	if [ -n "${vxlan}" ]; then
		script="qemu-ifup-vxlan"
		id=$(printf '%x' ${vxlan})
	fi

	[ -z "${script:-}" ] && echo "### ${nic} not supported, forgot to define the vxlan maybe? ###" && exit 1

	nics="${nics:-}\n    <interface type='ethernet' name='eth${i:=0}'>\n      <script path='/etc/libvirt/hooks/${script}'/>\n      <downscript path='/etc/libvirt/hooks/qemu-ifdown'/>\n      <mac address='${mac}'/>\n      <target dev='${MY_NAME}.${id}'/>\n      <model type='e1000'/>\n    </interface>"
	i=$(($i+1))
done



if [ -n "${DEV:-}" ]; then
	[ -z "${DEV%%*.qcow2}" ] && root_type=qcow2
	my_rootdisk="<disk type='file' device='disk'>\n      <driver name='qemu' type='${root_type:-raw}'/>\n      <source file='${DEV}'/>\n      <target dev='vda' bus='virtio'/>\n    </disk>"
else
	my_rootdisk="<disk type='network' device='disk'>\n      <driver name='qemu' type='raw'/>\n      <auth username='${MY_CEPH_USER}'>\n        <secret type='ceph' uuid='${MY_LIBVIRT_CEPH_ID}'/>\n      </auth>\n      <source protocol='rbd' name='${MY_CEPH_POOL}/${MY_NAME}'>\n        <config file='/etc/ceph/ceph.conf'/>\n      </source>\n      <target dev='vda' bus='virtio'/>\n    </disk>"
fi


sed -e "s|MY_CONF_BASE64|$(base64 -w0 ${MY_CONF})|" \
    -e "s|MY_INIT_BASE64|$(base64 -w0 ${MY_NAME}.init)|" \
    -e "s|MY_NAME|${MY_NAME}|" \
    -e "s|MY_BOOT|${boot:-}|" \
    -e "s|MY_NICS|${nics:-}|" \
    -e "s|MY_ROOTDISK|${my_rootdisk}|" \
    -e "s|MY_SECURE_BOOT|${SECURE_BOOT:-yes}|" \
    ${MY_LIBVIRT_TEMPLATE} >/tmp/${MY_NAME}.xml


if [ -n "${CLUSTER:-}" ]; then
	if [ -n "${boot:-}" ]; then
		rbd --cluster=${CLUSTER} --name=client.$(hostname) create ${MY_CEPH_POOL}/${MY_NAME} --size 2G \
		&& echo "### created blank drive ###" \
		|| /bin/true
	else
		rbd --cluster=${CLUSTER} --name=client.$(hostname) cp ${MY_CEPH_BASEPOOL}/bullseye ${MY_CEPH_POOL}/${MY_NAME} \
		&& rbd --cluster=${CLUSTER} --name=client.$(hostname) resize ${MY_CEPH_POOL}/${MY_NAME} --size 10G \
		&& echo "### copied golden image ###" \
		|| /bin/true
	fi
else
	echo "### set CLUSTER variable to have ceph rbds auto-create ###"
fi


if [ -n "${HV:=}" ]; then
	virsh -c qemu+tcp://${HV}/system create /tmp/${MY_NAME}.xml
	virsh -c qemu+tcp://${HV}/system console ${MY_NAME}
else
	echo "### set HV env var if you want to autostart vm ###"
fi
