#!/bin/bash
set -Eeuo pipefail

. ${MY_ENVFILE:-/etc/default/mygw}			## load central config, but optional for this script
. ${MY_LIB_COMMON:-/lib/mygw/common.sh}		## just for centralized default values, but optional for this script

[ -z "${MY_RACK:-}" ] && echo "MY_RACK is not defined" && exit 1
[ -n "$(ip vrf identify $$)" ] && echo "this should be started from the default vrf..." && exit 1

: ${MY_CEPH_DC:="mydc"}
: ${MY_CEPH_CLUSTER:="ceph"}

HNAME="$(hostname)"

## find drives
if [ -z "${MY_CEPH_OSDS:-}" ]; then
	: ${MY_CEPH_DRIVES:="/dev/sd[a-z]"}
	for drive in $(ls ${MY_CEPH_DRIVES}); do
		## this is the root drive, dont use that at all
		[ "${drive}" == "$(lsblk -pno PKNAME $(df --output=source / | grep '/dev/'))" ] && continue

		## no CEPH_DBS set but found ssd, using it as a DB drive
		[ -z "${MY_CEPH_DBS:-}" ] && [ "$(cat /sys/block/${drive##*/}/queue/rotational)" -eq 0 ] && dbdrive="${dbdrive:-}${drive} " && continue

		## none of the above, use it as regular osd drive
		dataosd="${dataosd:-}${drive} "
	done
	MY_CEPH_OSDS=${dataosd:-}
	MY_CEPH_DBS=${dbdrive:-}
fi



echo '####################################'
echo "initializing a *new* Ceph cluster *locally*"
echo '####################################'
echo
echo "cluster: ${MY_CEPH_CLUSTER}"
echo "hostname: ${HNAME}"
echo "myrack: ${MY_RACK}"
echo
echo '####################################'
echo
echo "OSDs: ${MY_CEPH_OSDS:=}"
echo "DBs:  ${MY_CEPH_DBS:=}"
echo
echo "WARN: **ALL** DRIVES (OSDs AND DBs) WILL BE  *** WIPED ***"
echo
echo "BE VERY VERY SURE"
echo

echo -n "should I go on? [y/n] - I only take 'yesYES' "
read CHECK
if [[ "$CHECK" != "yesYES" ]]; then
  echo "exiting..."
  exit 1
fi


### get osd bootsrap key
if [ ! -e "/var/lib/ceph/bootstrap-osd/${MY_CEPH_CLUSTER}.keyring" ]; then
	ceph --cluster=${MY_CEPH_CLUSTER} --name=client.${HNAME} auth get client.bootstrap-osd >/var/lib/ceph/bootstrap-osd/${MY_CEPH_CLUSTER}.keyring
	chown ceph.ceph /var/lib/ceph/bootstrap-osd/${MY_CEPH_CLUSTER}.keyring
	chmod 600 /var/lib/ceph/bootstrap-osd/${MY_CEPH_CLUSTER}.keyring
fi
##


echo '#####################################'
echo '##       OSDs: prep volumes        ##'
vgchange -qan
sleep 1
for drive in ${MY_CEPH_OSDS}; do
	wipefs -qa ${drive}
done

for drive in ${MY_CEPH_DBS}; do
	wipefs -qa ${drive}
	pvcreate ${drive}
done
vgcreate --addtag myceph ceph-db ${MY_CEPH_DBS}

for drive in ${MY_CEPH_OSDS}; do
	wipefs -qa ${drive}
	pvcreate ${drive}
	vgcreate --addtag myceph ceph-block-${drive##*/} ${drive}
	lvcreate -y -l 100%FREE -n ceph-block-${drive##*/} ceph-block-${drive##*/}

	if [ -n "${MY_CEPH_DBS}" ]; then
		extents=$(($(vgs --noheadings --options vg_extent_count ceph-db)/$(ls ${MY_CEPH_OSDS} | wc -w)))
		lvcreate -y -l ${extents} -n db-${drive##*/} ceph-db
		ceph-volume --cluster=${MY_CEPH_CLUSTER} lvm create --bluestore \
			--data ceph-block-${drive##*/}/ceph-block-${drive##*/} \
			--block.db ceph-db/db-${drive##*/}
	else
		ceph-volume --cluster=${MY_CEPH_CLUSTER} lvm create --bluestore --data ceph-block-${drive##*/}/ceph-block-${drive##*/}
	fi
done

# move my OSDs
ceph --cluster=${MY_CEPH_CLUSTER} --name=client.${HNAME} osd crush move ${HNAME%%.*} rack=${MY_RACK} datacenter=${MY_CEPH_DC} root=default
