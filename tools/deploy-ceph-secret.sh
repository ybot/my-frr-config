#!/bin/bash
set -Eeuo pipefail

#: ${OSDPERMS:='profile rbd pool=libvirt-pool, profile rbd-read-only pool=libvirt-base-images'}
: ${OSDPERMS:='profile rbd'}

[ -z "${CLUSTER:-}" ] && echo "please set CLUSTER var" && exit 1
[ -z "${LIBVIRTUSER:-}" ] && echo "please set LIBVIRTUSER var" && exit 1

for cephclient in $@
  do
	cat <<-EOF | virsh --connect=qemu+tcp://${cephclient}/system secret-define /dev/stdin
		<secret ephemeral='no' private='yes'>
		  <uuid>00000000-0000-0000-0000-000000000001</uuid>
		  <usage type='ceph'>
		    <name>${CLUSTER}.${LIBVIRTUSER}</name>
		  </usage>
		</secret>
	EOF
	MYKEY=$(ceph --cluster=${CLUSTER} --name=client.$(hostname) auth get-or-create-key client.${LIBVIRTUSER} mon 'profile rbd' osd "$OSDPERMS")
    virsh --connect=qemu+tcp://${cephclient}/system secret-set-value 00000000-0000-0000-0000-000000000001 ${MYKEY}
done

