#!/bin/bash
set -Eeuo pipefail

. ${ENVFILE:-/etc/default/mygw}
. ${LIBPATH:-/lib/mygw}/common.sh

: ${MY_ASN:="$(get_my_default_asn)"}


function install_license(){
	# Install license
	echo "$(date) INFO: Installing License..."
	echo $1 | /usr/cumulus/bin/cl-license -i
	return_code=$?
	if [ "$return_code" == "0" ]; then
		echo "$(date) INFO: License Installed."
		return 0
	else
		echo "$(date) ERROR: License not installed. Return code was: $return_code"
		/usr/cumulus/bin/cl-license
		return 1
	fi
}


## add default installer key
mkdir /home/cumulus/.ssh
echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMCUvBdM5PR+qdy3LahmeeD++6G9mkOT8ZqmHMKd1eOL' >/home/cumulus/.ssh/authorized_keys

## disable ssh password logins
sed -i -e 's/#*\s*PasswordAuthentication .*/PasswordAuthentication no/' /etc/ssh/sshd_config
systemctl restart sshd

## install cumulus license
install_license "${MY_CUMULUS_LICENSE}"
systemctl restart switchd.service

## Waiting for NCLU to finish starting up
last_code=1
while [ "1" == "$last_code" ]; do
	net show interface &> /dev/null
	last_code=$?
done



## deploy template config
net add hostname ${MY_HOSTNAME}
net add loopback lo ip address ${MY_NICS_LO_IPV4}
net add loopback lo ipv6 address ${MY_NICS_LO_IPV6}

net add routing prefix-list ipv4 ALL seq 5 permit 0.0.0.0/0 le 32
net add routing prefix-list ipv4 DEFAULT seq 5 permit 0.0.0.0/0
net add routing prefix-list ipv6 ALL seq 5 permit ::/0 le 128
net add routing prefix-list ipv6 DEFAULT seq 5 permit ::/0
net add routing route-map LOOPBACKv4 permit 10 match interface lo
net add routing route-map LOOPBACKv6 permit 10 match interface lo

net add bgp autonomous-system ${MY_ASN}
net add bgp router-id ${MY_NICS_LO_IPV4%%/*}
net del bgp default ipv4-unicast
net add bgp bestpath as-path multipath-relax
net add bgp bestpath compare-routerid
net add bgp neighbor fabric peer-group
net add bgp neighbor fabric remote-as external
net add bgp neighbor fabric capability extended-nexthop
net add bgp neighbor iBGP peer-group
net add bgp neighbor iBGP remote-as internal
net add bgp ipv4 unicast redistribute connected route-map LOOPBACKv4
net add bgp ipv4 unicast neighbor fabric activate
net add bgp ipv4 unicast neighbor fabric soft-reconfiguration inbound
net add bgp ipv4 unicast neighbor fabric addpath-tx-all-paths
net add bgp ipv4 unicast neighbor iBGP activate
net add bgp ipv4 unicast neighbor iBGP next-hop-self
net add bgp ipv4 unicast neighbor iBGP soft-reconfiguration inbound
net add bgp ipv4 unicast neighbor iBGP addpath-tx-all-paths
net add bgp ipv6 unicast redistribute connected route-map LOOPBACKv6
net add bgp ipv6 unicast neighbor fabric activate
net add bgp ipv6 unicast neighbor fabric soft-reconfiguration inbound
net add bgp ipv6 unicast neighbor fabric addpath-tx-all-paths
net add bgp ipv6 unicast neighbor iBGP activate
net add bgp ipv6 unicast neighbor iBGP next-hop-self
net add bgp ipv6 unicast neighbor iBGP soft-reconfiguration inbound
net add bgp ipv6 unicast neighbor iBGP addpath-tx-all-paths
net add bgp l2vpn evpn  neighbor fabric activate
net add bgp l2vpn evpn  neighbor iBGP activate

for iface in {1..49}; do
	net add interface swp${iface} mtu 9000
	net add bgp neighbor swp${iface} interface peer-group fabric
done

net commit description "ztp autodeploy commit"
